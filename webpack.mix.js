let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/app.js', 'public/js/app.js');
mix.js('resources/assets/js/admin.js', 'public/js/admin.js');


mix.scripts([

    'resources/assets/js/mdb.min.js',
    'resources/assets/js/popper.min.js',
    'resources/assets/js/myscript.js'
], 'public/js/frontend.js');


mix.scripts([
    'resources/assets/js/devs/modernizr.custom.js',
    'resources/assets/js/devs/wow.min.js',
    'resources/assets/js/devs/Chart.js',
    'resources/assets/js/devs/underscore-min.js',
    'resources/assets/js/devs/moment-2.2.1.js',
    'resources/assets/js/devs/clndr.js',
    'resources/assets/js/devs/site.js',
    'resources/assets/js/devs/metisMenu.min.js',
    'resources/assets/js/devs/custom.js',
    'resources/assets/js/devs/classie.js'
], 'public/js/backend/backend.js');


mix.sass('resources/assets/sass/app.scss', 'public/css/app.css');
mix.sass('resources/assets/sass/app2.scss', 'public/css/app2.css');

mix.styles([
    'resources/assets/sass/backend_css/animate.css',
    'resources/assets/sass/backend_css/clndr.css',
    'resources/assets/sass/backend_css/custom.css',
    'resources/assets/sass/backend_css/jqvmap.css',
    'resources/assets/sass/backend_css/style.css',


], 'public/css/backend/backend.css');

mix.styles([
    'resources/assets/sass/font-awesome.min.css',
    'resources/assets/sass/mdb.min.css',
    'resources/assets/sass/style.css'
    // 'resources/assets/sass/rate.css',



], 'public/css/frontend.css');