<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//@Author: Kyien

// Backend Routes



// ALLRoutes
/*//////////////////////////////////////////////////////*/
//product routes
//get all products
Route::get('/products',[
    'as'=> 'products',
    'uses'=> 'productscontroller@get_products'
    ]);
Route::group(['middleware' => ['jwt.verify']], function() {

//post products
Route::post('/add/product',[
    'as'=>'add_product',
    'uses'=> 'productscontroller@add_product'
]);

//update product
Route::put('/update/product/{id}',[
    'as'=>'update_product',
    'uses'=> 'productscontroller@update_product'
]);

Route::delete('/product/delete/{id}',[
    'as'=> 'delete_product',
    'uses'=> 'productscontroller@delete_product'
    ]);
     //post rating
    Route::post('/product/rating',[
        'as'=>'product_rating',
        'uses'=>'ProductReviewController@storeReviewForProduct'
        ]);
    });
        //get product review
        Route::get('/product/review/{prod_id}',
        ['as'=>'get_reviews',
          'uses'=>'ProductReviewController@get_reviews']);

/*//////////////////////////////////////////////////////*/
//categories
//get categories
Route::get('/categories',[
    'as'=> 'categories_get',
    'uses'=> 'productscontroller@get_categories'
]);
Route::group(['middleware' => ['jwt.verify']], function() {

//add category

Route::post('/category/add',[
    'as'=> 'category_add',
    'uses'=> 'productscontroller@add_category'
]);

//delete category
Route::delete('/category/delete/{id}',[
    'as'=> 'category_delete',
    'uses'=> 'productscontroller@delete_category'
]);

});
//Authenticationcontroller
Route::post('/auth/login',[
    'as'=>'user_login',
    'uses'=>'AuthController@userlogin'
]);

Route::post('/auth/registration',[
    'as'=>'user_registration',
    'uses'=>'AuthController@user_register'
]);


/*//////////////////////////////////////////////////////*/
//order routes
