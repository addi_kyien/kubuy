/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap2');
require('./bootstrap_3_3_4');



window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//front end components
import VueRouter from 'vue-router'
import Veevalidate from 'vee-validate'
import VueToastr from '@deveodk/vue-toastr'
import VModal from 'vue-js-modal'

// import Vuex from 'vuex'
// You need a specific loader for CSS files like https://github.com/webpack/css-loader
// If you would like custom styling of the toastr the css file can be replaced
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'



//backend components
import Dash from './components/backend/Dash.vue'
import home from './components/home.vue'
import AddProduct from './components/backend/AddProduct.vue'
import allProducts from './components/backend/allProducts.vue'
import AddCategory from './components/backend/add_category.vue'
import Root from './components/backend/root.vue'
import store from './components/backend/store/store'
import Stretch from './components/Circle2.vue'
import Login from './components/backend/login.vue'

Vue.use(VueRouter)
Vue.use(VModal)
Vue.use(Veevalidate)
Vue.use(VueToastr, {
    defaultPosition: 'toast-top-right',
    defaultTimeout: 2000
})

Vue.component('stretch', Stretch)
    // Vue.component('root', Root)

const routes = [{
        path: '/backend/admin',
        component: Dash,
        name: 'dash',
        meta: { requiresAuth: true, adminAuth: true }
    },
    {
        path: '',
        component: home,
        name: 'home'
    },
    {
        path: '/backend/admin/add_product',
        component: AddProduct,
        name: 'add_product',
        meta: { requiresAuth: true, adminAuth: true }
    },
    {
        path: '/backend/admin/login',
        component: Login,
        name: 'login',
        meta: { noAuth: true }

    },
    {
        path: '/backend/admin/all_products',
        component: allProducts,
        name: 'allProducts',
        meta: { requiresAuth: true, adminAuth: true }
    },
    {
        path: '/backend/admin/add_category',
        component: AddCategory,
        name: 'add_category',
        meta: { requiresAuth: true, adminAuth: true }
    },


    // fall-back  redirect url
    { path: '*', redirect: { name: 'dash' } }



];



const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: 'active-class'
});

router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        let authUser = JSON.parse(window.localStorage.getItem('authuser'))
        let authtoken = window.localStorage.getItem('authusertoken')
        if (!authUser || !authtoken) {
            next({ name: 'login' })
        } else if (to.meta.adminAuth) {
            if (authUser.role === 'admin') {
                next()
            } else {
                next({ name: 'home' })
            }
        }

    }
    // else {
    //     next()
    // }
    if (to.meta.noAuth) {
        let authtoken = JSON.parse(window.localStorage.getItem('authusertoken'))
        if (!authtoken) {
            next()
        } else {
            next(false)
        }

    }
    // next()
})



var app = new Vue({
    el: '#app',
    render: h => h(Root),
    router,
    store

});