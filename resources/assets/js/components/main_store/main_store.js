import Vue from 'vue'
import Vuex from 'vuex'


import * as types from './mutation-types'


Vue.use(Vuex)


const store = new Vuex.Store({


    state: {

        // bought items
        cart: [],
      
        //allcategories
        categories: [],

        // ajax loader
        showLoader: false,

        isLoggedIn: false,

        // all products
        products: [],
 
        User: {}

    },
    getters: {
        // All products
        allProducts: (state) => {
            return state.products
        },



        categories: (state) => {
            return state.categories
        },

        loader: (state) => {

            return state.showLoader
        },
        cartItems:(state) =>{

            return state.cart
        }
    },

    mutations: {


        [types.LOGIN]: (state) => {


            state.showLoader = true

        },
        [types.LOGIN_SUCCESS]: (state, payload) => {

            state.showLoader = false
            state.isLoggedIn = true
            state.User = payload
        },
        [types.LOGOUT_SUCCESS]: (state) => {

            // state.adminUser =
            state.isLoggedIn = false
        },


        [types.ALL_PRODUCTS]: (state) => {
            // Called when fetching products
            state.showLoader = true
        },
        [types.ALL_PRODUCTS_SUCCESS]: (state, payload) => {
            // Called when products have been fetched
            state.showLoader = false
                // Updates state products
            state.products = payload.products
            // state.product_reviews = payload.product_reviews
                

        },







        [types.ALL_CATEGORIES]: (state) => {
            state.showLoader = true
        },
        [types.ALL_CATEGORIES_SUCCESS]: (state, payload) => {
            state.showLoader = false
            state.categories = payload
        },


        [types.ADD_TO_CART]: (state, payload) => state.cart.push(payload),
        [types.REMOVE_FROM_CART]: (state, payload) => {
            const index = state.cart.findIndex(p => p.id === payload)
            state.cart.splice(index, 1)
            console.log(state.cart, state.cart.length, index)
        }

    },

    actions: {

        login({ commit }, creds) {

            let AuthToken = ''
            axios.post(route('user_login'), creds).then(response => {
                // console.log(response.data)

                if (AuthToken = response.data.token) {

                    window.localStorage.setItem('authuser', JSON.stringify(response.data.user))
                    window.localStorage.setItem('authusertoken', AuthToken)

                }

                commit(types.LOGIN_SUCCESS, response.data.user)

            }).catch(error => console.log(error))


        },
        logout({ commit }) {

            window.localStorage.removeItem('authusertoken')
            window.localStorage.removeItem('authuser')
            commit(types.LOGOUT_SUCCESS)
        },

        allProducts({ commit }) {
            commit(types.ALL_PRODUCTS)
                // Fetch actual products from the API
            axios.get(route('products')).then(response => {

                commit(types.ALL_PRODUCTS_SUCCESS, response.data)
            }).catch(error => console.log(error))
        },




        allcategories({ commit }) {
            // commit(ALL_CATEGORIES)
            // Fetch all manufacturers from API
            axios.get(route('categories_get')).then(response => {
                commit(types.ALL_CATEGORIES_SUCCESS, response.data.categories)

            }).catch(error => console.log(error))
        },

        add_to_cart({commit}, item){

            commit(types.ADD_TO_CART,item)
        },
        remove_from_cart({commit}, itemid){

            commit(types.REMOVE_FROM_CART,itemid)
        }

        


    }


})



export default store;