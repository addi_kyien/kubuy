import Vue from 'vue'
import Vuex from 'vuex'
// import createPersistedState from 'vuex-persistedstate'
//importing plugin to enable store sync across browser tabs and/ windows
// import createMutationsSharer from 'vuex-shared-mutations'
// import VuexPersist from 'vuex-persist';
import createStorageSync from 'vuex-storage-sync'


import * as types from './mutation-types'

Vue.use(Vuex)


const store = new Vuex.Store({
    // strict: true,

    plugins: [createStorageSync({
        mutationKey: 'MY_VUEX',
        snapshotKey: 'MY_SNAPSHOT'
    })],
    // plugins: [createMutationsSharer({
    //     predicate: ['type.ADD_CATEGORY_SUCCESS', 'type.ADD_PRODUCT_SUCCESS'],
    //     sharingKey: 'MY-VUEX'
    // })],

    state: {


        //allcategories
        categories: [],

        // ajax loader
        showLoader: false,

        isLoggedIn: false,

        // all products
        products: [],

        adminUser: {}

    },
    getters: {
        // All products
        allProducts: (state) => {
            return state.products
        },



        categories: (state) => {
            return state.categories
        },

        loader: (state) => {

            return state.showLoader
        }
    },

    mutations: {


        [types.LOGIN]: (state) => {


            state.showLoader = true

        },
        [types.LOGIN_SUCCESS]: (state, payload) => {

            state.showLoader = false
            state.isLoggedIn = true
            state.adminUser = payload
        },
        [types.LOGOUT_SUCCESS]: (state) => {

            // state.adminUser =
            state.isLoggedIn = false
        },


        [types.ALL_PRODUCTS]: (state) => {
            // Called when fetching products
            state.showLoader = true
        },
        [types.ALL_PRODUCTS_SUCCESS]: (state, payload) => {
            // Called when products have been fetched
            state.showLoader = false
                // Updates state products
            state.products = payload

        },

        [types.ADD_PRODUCT]: (state) => {
            // ...Same pattern
            state.showLoader = true
        },
        [types.ADD_PRODUCT_SUCCESS]: (state, payload) => {
            state.showLoader = false
                //making a copy so as to get new reference

            state.products.push(payload)
                // Vue.set(state, 'products', state.products)
            console.log(state.products)
        },
        [types.UPDATE_PRODUCT]: (state, payload) => {
            state.showLoader = true
        },
        [types.UPDATE_PRODUCT_SUCCESS]: (state, payload) => {
            state.showLoader = false
            state.products = state.products.map(p => {
                if (p._id === payload._id) {
                    payload = { payload }
                    return payload
                }
                return p
            })
        },
        [types.REMOVE_PRODUCT]: (state, payload) => {
            state.showLoader = true
        },
        [types.REMOVE_PRODUCT_SUCCESS]: (state, payload) => {
            state.showLoader = false
            const index = state.products.findIndex(p => p._id === payload)
            console.debug('index', index)
            state.products.splice(index, 1)

        },


        [types.ALL_CATEGORIES]: (state) => {
            state.showLoader = true
        },
        [types.ALL_CATEGORIES_SUCCESS]: (state, payload) => {
            state.showLoader = false
            state.categories = payload
        },
        [types.ADD_CATEGORY]: (state) => {
            // ...Same pattern
            state.showLoader = true
        },
        [types.ADD_CATEGORY_SUCCESS]: (state, payload) => {
            state.showLoader = false

            state.categories.push(payload)
            console.log(state.categories)
        },



        // [types.ADD_TO_CART]: (state, payload) => state.cart.push(payload),
        // [types.REMOVE_FROM_CART]: (state, payload) => {
        //     const index = state.cart.findIndex(p => p._id === payload)
        //     state.cart.splice(index, 1)
        //     console.log(state.cart, state.cart.length, index)
        // }

    },

    actions: {

        login({ commit }, creds) {

            let AuthToken = ''
            axios.post(route('user_login'), creds).then(response => {
                // console.log(response.data)

                if (AuthToken = response.data.token) {

                    window.localStorage.setItem('authuser', JSON.stringify(response.data.user))
                    window.localStorage.setItem('authusertoken', AuthToken)

                }

                commit(types.LOGIN_SUCCESS, response.data.user)

            }).catch(error => console.log(error))


        },
        logout({ commit }) {

            window.localStorage.removeItem('authusertoken')
            window.localStorage.removeItem('authuser')
            commit(types.LOGOUT_SUCCESS)
        },

        allProducts({ commit }) {
            commit(types.ALL_PRODUCTS)
                // Fetch actual products from the API
            axios.get(route('products')).then(response => {

                commit(types.ALL_PRODUCTS_SUCCESS, response.data.products)
            }).catch(error => console.log(error))
        },

        addProduct({ commit }, payload) {
            return new Promise((resolve, reject) => {
                commit(types.ADD_PRODUCT)
                    // Create a new product via API
                axios.post(route('add_product'), payload).then(response => {
                    console.log(response.data)
                    if (response.data.product) {
                        commit(types.ADD_PRODUCT_SUCCESS, response.data.product)
                        resolve();
                    }
                }).catch(error => console.log(error))
            })
        },
        updateProduct({ commit }, payload) {
            commit(types.UPDATE_PRODUCT)
                // Update product via API
            axios.put(route('update_product', { id: payload.id }), payload).then(response => {
                commit(types.UPDATE_PRODUCT_SUCCESS, response.data)
            }).catch(error => console.log(error))
        },
        removeProduct({ commit }, prod_id) {
            commit(types.REMOVE_PRODUCT)
                // Delete product via API
            axios.delete(route('delete_product', { id: prod_id }), prod_id).then((response) => {
                console.debug('response', response.data)
                commit(types.REMOVE_PRODUCT_SUCCESS, response.data)

            }).catch((error) => console.log(error))

        },


        allcategories({ commit }) {
            // commit(ALL_CATEGORIES)
            // Fetch all manufacturers from API
            axios.get(route('categories_get')).then(response => {
                commit(types.ALL_CATEGORIES_SUCCESS, response.data.categories)

            }).catch(error => console.log(error))
        },
        addcategory({ commit }, payload) {
            return new Promise((resolve, reject) => {
                commit(types.ADD_CATEGORY)
                    //create new category via API
                axios.post(route('category_add'), payload).then(response => {
                        console.log(response.data.message)
                        commit(types.ADD_CATEGORY_SUCCESS, response.data.category)
                        resolve();
                    })
                    .catch(error => console.log(error))
            })
        }



    }


})



export default store;