//Mutation constants to avoid type errors
import * as types from './mutation-types'
// import { Vue } from 'vue';

export const productMutations = {
    [types.ALL_PRODUCTS]: (state) => {
        // Called when fetching products
        state.showLoader = true
    },
    [types.ALL_PRODUCTS_SUCCESS]: (state, payload) => {
        // Called when products have been fetched
        state.showLoader = false
            // Updates state products
        state.products = Object.assign({}, payload);

    },
    // [PRODUCT_BY_ID]: (state) => {
    //     // Called when fetching products by ID
    //     state.showLoader = true
    // },
    // [PRODUCT_BY_ID_SUCCESS]: (state, payload) => {
    //     // Called when product is fetched
    //     state.showLoader = false
    //         // Updates state product
    //     state.product = payload
    // },
    [types.ADD_PRODUCT]: (state) => {
        // ...Same pattern
        state.showLoader = true
    },
    [types.ADD_PRODUCT_SUCCESS]: (state, payload) => {
        state.showLoader = false
            //making a copy so as to get new reference
            // const products = Array.from(state.products)
        state.products.push(payload)
            // Vue.set(state, 'products', state.products)
        console.log(state.products)
    },
    [types.UPDATE_PRODUCT]: (state, payload) => {
        state.showLoader = true
    },
    [types.UPDATE_PRODUCT_SUCCESS]: (state, payload) => {
        state.showLoader = false
        state.products = state.products.map(p => {
            if (p._id === payload._id) {
                payload = { payload }
                return payload
            }
            return p
        })
    },
    [types.REMOVE_PRODUCT]: (state, payload) => {
        state.showLoader = true
    },
    [types.REMOVE_PRODUCT_SUCCESS]: (state, payload) => {
        state.showLoader = false
        const index = state.products.findIndex(p => p._id === payload)
        console.debug('index', index)
        state.products.splice(index, 1)
    }
}

export const categoryMutations = {
    [types.ALL_CATEGORIES]: (state) => {
        state.showLoader = true
    },
    [types.ALL_CATEGORIES_SUCCESS]: (state, payload) => {
        state.showLoader = false
        state.categories = Object.assign({}, payload);
    },
    [types.ADD_CATEGORY]: (state) => {
        // ...Same pattern
        state.showLoader = true
    },
    [types.ADD_CATEGORY_SUCCESS]: (state, payload) => {
        state.showLoader = false
            // const categories = Array.from(state.categories)

        state.categories.push(payload)
        console.log(state.categories)
            // Vue.set(state, 'categories', state.categories)
    },
}

export const cartMutations = {
    [types.ADD_TO_CART]: (state, payload) => state.cart.push(payload),
    [types.REMOVE_FROM_CART]: (state, payload) => {
        const index = state.cart.findIndex(p => p._id === payload)
        state.cart.splice(index, 1)
        console.log(state.cart, state.cart.length, index)
    }
}