import {
    ADD_PRODUCT,
    ADD_PRODUCT_SUCCESS,
    ADD_CATEGORY,
    ADD_CATEGORY_SUCCESS,
    PRODUCTS_BY_CATEGORY,
    PRODUCTS_BY_CATEGORY_SUCCESS,
    // PRODUCT_BY_ID,
    // PRODUCT_BY_ID_SUCCESS,
    UPDATE_PRODUCT,
    UPDATE_PRODUCT_SUCCESS,
    REMOVE_PRODUCT,
    REMOVE_PRODUCT_SUCCESS,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    ALL_PRODUCTS,
    ALL_PRODUCTS_SUCCESS,
    ALL_CATEGORIES,
    ALL_CATEGORIES_SUCCESS

} from './mutation-types'

export const productActions = {
    allProducts({ commit }) {
        commit(ALL_PRODUCTS)
            // Fetch actual products from the API
        axios.get(route('products')).then(response => {

            commit(ALL_PRODUCTS_SUCCESS, response.data.products)
        }).catch(error => console.log(error))
    },
    // productById({ commit }, payload) {
    //     commit(PRODUCT_BY_ID)
    //         // Fetch product by ID from API
    //     axios.get(route()).then(response => {
    //         commit(PRODUCT_BY_ID_SUCCESS, response.data)
    //     })
    // },
    addProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            commit(ADD_PRODUCT)
                // Create a new product via API
            axios.post(route('add_product'), payload).then(response => {
                console.log(response.data)
                if (response.data.product) {
                    commit(ADD_PRODUCT_SUCCESS, response.data.product)
                    resolve();
                }
            }).catch(error => console.log(error))
        })
    },
    updateProduct({ commit }, payload) {
        commit(UPDATE_PRODUCT)
            // Update product via API
        axios.put(route('update_product', { id: payload.id }), payload).then(response => {
            commit(UPDATE_PRODUCT_SUCCESS, response.data)
        }).catch(error => console.log(error))
    },
    removeProduct({ commit }, payload) {
        commit(REMOVE_PRODUCT)
            // Delete product via API
        axios.delete(route('delete_product', { id: payload.id }), payload).then((response) => {
            console.debug('response', response.data)
            commit(REMOVE_PRODUCT_SUCCESS, response.data)

        }).catch((error) => console.log(error))
    }
}

export const categoryActions = {
    allcategories({ commit }) {
        // commit(ALL_CATEGORIES)
        // Fetch all manufacturers from API
        axios.get(route('categories_get')).then(response => {
            commit(ALL_CATEGORIES_SUCCESS, response.data.categories)

        }).catch(error => console.log(error))
    },
    addcategory({ commit }, payload) {
        commit(ADD_CATEGORY)
            //create new category via API
        axios.post(route('category_add'), payload).then(response => {
                console.log(response.data.message)
                commit(ADD_CATEGORY_SUCCESS, response.data.category)
            })
            .catch(error => console.log(error))

    }
}