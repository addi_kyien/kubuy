export const productGetters = {
    // All products
    allProducts: (state) => {
        return state.products
    },
    //products by category
    productsBycategory: (state, getters) => category_id => {

        if (getters.allProducts.length > 0) {
            return getters.allProducts.filter(p => p.category_id === category_id)[0]
        } else {
            return state.product
        }
    },
    // Get Product by ID
    productById: (state, getters) => id => {
        if (getters.allProducts.length > 0) {
            return getters.allProducts.filter(p => p.id === id)[0]
        } else {
            return state.product
        }
    },
}
export const categoryGetters = {

    categories: (state) => {
        return state.categories
    }
}
export const loaderstatus = {

    loader: (state, getters) => {

        return state.showLoader
    }

}