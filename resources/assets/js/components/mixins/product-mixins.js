export const ProductMixin = {

    methods: {

        ls_get_products() {
            return localStorage.getItem('myvuex')
        },
        ls_attachlistener(callback) {

            localStorage.on('myvuex', callback)

        }
    }
}