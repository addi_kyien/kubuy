/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
require('./bootstrap.min');
// require('./flexslider');
// require('./myflex');


window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// global dependencies
import VueRouter from 'vue-router'
import VueToastr from '@deveodk/vue-toastr'
// import VueCarousel from 'vue-carousel'
import StarRating from 'vue-star-rating'
// import chunk  from 'chunk'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'

//front end components
import home from './components/home.vue'
import about from './components/about.vue'
import product from './components/product.vue'
import product_category from './components/product_category.vue'
import App from './components/app.vue'
import store from './components/main_store/main_store'
import Autocomp from './components/search.vue'
import CartModal from './components/cartModal.vue'
// import { Stretch } from 'vue-loading-spinner'

//globalcomponents
Vue.component('star-rating', StarRating)
Vue.component('cart-modal', CartModal)

//css

Vue.use(VueRouter);
// Vue.use(VueCarousel);
Vue.use(VueToastr, {
    defaultPosition: 'toast-top-right',
    defaultTimeout: 2000
})

const routes = [{
        path: '',
        component: home,
        name: 'home'
    },
    { path: '/about', component: about, name: 'about' },
    { path: '/product_category/:id', component: product_category, name: 'prod_cat' },
    { path: '/product/:id', component: product, name: 'product' },

    // fall-back  redirect url
    { path: '*', redirect: { name: 'home' } }



];



const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: 'active-class'
});



var app1 = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});