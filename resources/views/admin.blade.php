<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
     <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KUBUY Admin</title>


                 
                 <link href="{{asset('css/app2.css')}}" rel="stylesheet" type="text/css">  
               <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css"> 
               <link href="{{asset('css/backend/backend.css')}}" rel="stylesheet" type="text/css">

              
	 

</head>
<body class="cbp-spmenu-push">

	 

<div id="app">
              
	<Root></Root>
             
</div>

    
<!--footer-->
	
		    <!-- scripts-->
			 @routes
	<script src="{{asset('js/admin.js')}}"></script>
	<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
			]); ?>
			
		</script>
		
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
			showLeftPush = document.getElementById( 'showLeftPush' ),
			body = document.body;
			
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
		
		<script src="{{asset('js/backend/backend.js')}}"></script>
		<script>
			new WOW().init();
		</script>
		

	
</body>
</html>