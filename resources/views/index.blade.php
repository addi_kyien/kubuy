<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>
        
               <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css"> 
               {{--  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">   --}}
            <link href="{{asset('css/frontend.css')}}" rel="stylesheet" type="text/css">
       

      
    </head>
    <body>
    
            <div id="app">
          
  <App></App>
                
            </div>
    

            @routes

        
         <script src="{{asset('js/popper.min.js')}}"></script>
         <script src="{{asset('js/app.js')}}"></script>
         <!-- Enable bootstrap 4 theme -->

        
         <script src="{{asset('js/mdb.min.js')}}"></script>
         <!-- <script src="{{asset('js/myscript.js')}}"></script> -->
         
         <script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
    </body>
</html>
