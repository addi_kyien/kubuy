<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('user_id');
            $table->float('rating');
            $table->text('comment');
            $table->tinyInteger('approved')->default('1');
            $table->tinyInteger('spam')->default('0');
            $table->timestamps();


            $table->foreign('user_id')
      ->references('id')->on('users')
      ->onDelete('cascade');
      $table->foreign('product_id')
      ->references('id')->on('products')
      ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
