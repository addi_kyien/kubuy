<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    //
    protected function userlogin(Request $request){

        $this->validate($request,[
            'email' => 'required|email',
            'password'=> 'required'
        ]);
        $credentials = $request->only('email', 'password');
        $token = JWTAuth::attempt($credentials);
        try{

            if ( ! $token ) {
                return response()->json([
                    'error'=>'Invalid Credentials'
                ],401);
                    
            }

        }catch(JWTException $e){
            return response()->json([
                'error'=> 'could not create token!'
            ],500);
        }
        // $user = JWTAuth::parseToken()->authenticate();
        $user= Auth::user();
    return response()->json([
        'token' =>$token,
        'user'=>$user
    ],200);


    }

    protected function user_register(Request $request){

     $this->validate($request,[
            'firstname'=>'required|string|max:255',
            'lastname'=>'required|string|max:255',
            'email' =>'required|email|unique:users',
            'password'=>'required|min:6',
            'phone' =>'required|numeric',
            'location'=>'required|alpha_dash'
        ]);
        // if ($success){
        //     return response()->json([
        //         'message' => 'validation successful'
        //     ]);
        // }

        $user=User::create([
            'firstname'=> $request->firstname,
            'lastname'=> $request->lastname,
            'email' => $request->email,
            'password'=> bcrypt($request->password),
            'phone' => $request->phone,
            'location'=> $request->location,
            'role'=>'admin',
            'avatar'=>'avatar_default.jpg'

        ]);
            if(!$user){
                return response()->json([
                    'message'=>'could not create user',
                    
                ],500);
            }

            return response()->json([
                'message'=>'user created successfully',
                'user'=>$user
            ],200);

            

    }



}
