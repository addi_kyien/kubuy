<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Product;

class ProductReviewController extends Controller
{
    //
    // public function __construct(){

    //     $this->middleware('auth');
    // }

    public function storeReviewForProduct(Request $req)
            {
        //    $product=Product::find($req->product_id);
            $review=new Review();
            // $review->product_id= $req->product_id;
            $review->user_id=$req->user_id;
            $review->rating=$req->rating;   
            $review->comment=$req->comment;   

            $product = Product::find($req->product_id);
          
            // $product->reviews()->sync();
            $product->reviews()->save($review);

            // recalculate ratings for the specified product
            $product->recalculateRating();
                return response()->json([
                    'message'=>'success'
                ],200);
            }
            public function get_reviews($prod_id){
            $product_reviews=Product::with('users.reviews')
             ->where('product_id',$prod_id)->get();
                return response()->json([
                    'reviews' =>$product_reviews
                ],200);
                
            }
        }