<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth;
use App\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Review;
use Validator;
use Image;


class productscontroller extends Controller
{
    //adding product
    public function add_product(Request $req){
           
       $validator= Validator::make($req->all(),[
            'name' => 'required|alpha_dash|max:255',
            'description' => 'required|max:255',
            'quantity' => 'required|numeric|integer|min:1',
            'price' => 'required|regex:/^\d*(\.\d{2})?$/'
            // 'photo' => 'required|image|mimes:jpeg,png|max:2048'
        ]);

        if ($validator->fails()) {    
            return response()->json(['errors'=>$validator->messages()], 200);
        }
    //   \Log::info($req->photo);
        //image validation
        if($req->photo){
            
            $exploded= explode(',',$req->photo);

            $image=base64_decode($exploded[1]);

            if(str_contains($exploded[0],'jpeg')){

                $extension= 'jpg';
            }else
            {
                $extension='png';
            }
            $filename= time() . '.'.$extension;
     
            Storage::disk('uploads')->put('products/'.$filename,$image);
            
                }

        $product=Product::create([
            'name' => $req->name,
            'description' =>$req->description,
            'quantity'=> $req->quantity,
            'image' => $filename,
            'category_id' =>$req->category,
            'price' =>$req->price
        ]);

        if($product){

            return response()->json([
                'message'=>'product inserted successfully!', 
                'product' => $product
            ],200);
        }
        else{
             return response()->json([
                'message'=>'failure inserting product!'
            ]);
        }

    }

    //getting products
     
    public function get_products(){

        $products=Product::all();


        return response()->json( [
            'products' => $products,
            
        ],200);

    }

    //update product

    public function update_product(Request $req,$id){


        $validator= $this->validate($req,[
            'name' => 'required|alpha_dash|max:255',
            'description' => 'required|max:255',
            'quantity' => 'required|numeric|integer|min:1',
            'price' => 'required|regex:/^\d*(\.\d{2})?$/',
            'image' => 'required|image|mimes:jpeg,png|max:2048'
        ]);

        // if ($validator->fails()) {    
        //     return response()->json(['errors'=>$validator->messages()]);
        // }
            //image validation
            if($req->hasFile('image')){

                $image=$req->file('image');
                    //validation
                  
                    
                    $original_filename=$image->getClientOriginalName();
                    // $file_extension=$image->getClientOriginalExtension();
                    // $image_name=$original_filename.'.'. $file_extension;
                    $image_path=Storage::disk('uploads')->putFile('products',$original_filename);

                
                    


                }

        $product=Product::findorfail($id);

            //product find unsuccessful

            if(!$product){
                return response()->json([
                    'message' => 'could not find product'
                ]);
            }
        //product find successful
            $update_product=$product->update([
                'name' => $req->name,
                'description' =>$req->description,
                'quantity'=> $req->quantity,
                'image' => $image_path,
                'category_id' =>$req->category_id,
                'price' =>$req->price
            ]);

            if($update_product= false){

                return response()->json([
                    'message' => 'product update failed'
                 ]);

            }
            return response()->json([
                'message' => 'product updated successfully'
            ]);

            }
            
            public function delete_product($id){
                $product=Product::findorfail($id);

                if(!$product){
                    return response()->json([
                        'message' => 'could not find product'
                    ]);
                }
                else{
                $product->delete();

                return response()->json([
                    'message '=> 'delete successful'
                ]);
                }

            }
    //fetch categories

                public function get_categories(){

                    $categories=Category::all();

                    return response()->json([
                        'categories' => $categories
                    ]);
                    
                }

        //add category

                public function add_category(Request $req){
               
                                $validator=Validator::make($req->all(), [
                                    'name' => 'required|alpha_dash|max:255',
                                    'description' => 'required|max:255'
                                ]);

                                if ($validator->fails()) {    
                                    return response()->json(['errors'=>$validator->errors()->all()]);
                                }

                                $category=Category::create([
                                    'name'=> $req->name,
                                    'description'=> $req->description
                                            ]);

                                if(!$category){
                                    return response()->json([
                                        'message' => 'could not create category!'
                                    ]);
                                }

                                else{
                                    return response()->json([
                                    'message'=>'category inserted successfully',
                                    'category'=>$category
                                   
                                ]);
                                }

           
                 }

                 //delete category

                public function delete_category($id){

                        $category=Category::findorfail($id);

                        if(!$category){
                            return response()->json([
                                'message' => 'coul not find category'
                            ]);
                        }
                        else{
                        $category->delete();

                        return response()->json([
                            'message '=> 'delete successful'
                        ]);
                        }
                    }
                


    }
