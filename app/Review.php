<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //

    protected $fillable=['product_id','user_id','rating','approved','spam','comment'];

    public function users()
    {
      return $this->belongsTo('App\User');
    }
  
    public function products()
    {
      return $this->belongsTo('App\Product');
    }
  
    public function scopeApproved($query)
    {
      return $query->where('approved', true);
    }
  
    public function scopeSpam($query)
    {
      return $query->where('spam', true);
    }
  
    public function scopeNotSpam($query)
    {
      return $query->where('spam', false);
    }
}
