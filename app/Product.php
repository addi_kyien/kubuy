<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    //
    use SoftDeletes;

     protected $fillable = [
        'name', 'description', 'price','image','category_id','quantity','rating_count','rating_caches'
    ];

    public function users(){

        return $this->belongsToMany('App\User');
    }

    public function categories(){

        return $this->belongsTo('App\Category');
    }
    public function reviews(){

        return $this->hasMany('App\Review');
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

  public function recalculateRating()
  {
    $reviews = $this->reviews()->notSpam()->approved();
    $avgRating = $reviews->avg('rating');
    $this->rating_cache = round($avgRating,1);
    $this->rating_count = $reviews->count();
    $this->save();
  }
}
