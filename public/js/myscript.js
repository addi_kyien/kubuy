$.fn.followTo = function(pos) {
    var $this = this,
        $window = $(window);

    $window.scroll(function(e) {
        if ($window.scrollTop() > pos) {
            $this.css({
                position: 'absolute',
                width: '60%'
            });
        } else {
            $this.css({
                position: 'fixed',
                width: '20%'
            });
        }
    });
};

$('.sidebar-fixed-left ').followTo(950);

// $('.dropdown-toggle').dropdown();